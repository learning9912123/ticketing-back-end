CREATE TABLE if not exists  `schedule`(

  `id` int NOT NULL AUTO_INCREMENT,

  `cinema_id` int  NOT NULL,

  `room_id` int  NOT NULL,

  `movie_id` int  NOT NULL,

  `begin_time` datetime  NOT NULL,

  `end_time` datetime  NOT NULL,

  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`)

);

CREATE TABLE  if not exists `customer`(
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);


CREATE TABLE if not exists  `movie` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `duration` VARCHAR(16) NOT NULL,
  `description` VARCHAR(500) NOT NULL,
  `create_time` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

CREATE TABLE if not exists `room`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(32)  NOT NULL,
  `style` varchar(255)  NOT NULL,
  `cinema_id` int NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

CREATE TABLE if not exists `ticket`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `schedule_id` int NOT NULL,
  `seat_id` int NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

CREATE TABLE if not exists `cinema` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `longitude` DECIMAL(10,6) NOT NULL,
  `latitude` DECIMAL(10,6) NOT NULL,
  `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
 );

CREATE TABLE if not exists `seat`  (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cinema_id` int NOT NULL,
  `room_id` int NOT NULL,
  `row` int NOT NULL,
  `col` int NOT NULL,
  `status` int NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);